# Presentation
KLanguageTool \[keɪ læŋ.ɡwɪdʒ tuːl\] is a plugin for [KTextEditor](https://api.kde.org/frameworks/ktexteditor/html/) providing spell and grammar checking relying on [LanguageTool](https://www.languagetool.org/).

# Configuring
Simple:
```sh
cmake .
```

Recommended for a working installation (correct options may change depending on your system):
```sh
cmake -D CMAKE_INSTALL_PREFIX=/usr -D KDE_INSTALL_USE_QT_SYS_PATHS:BOOL=ON .
```

Distribution packagers are responsive for ensuring the installation directories are good for their system.

# Building
```sh
make
```

# Installing
```sh
make install
```

# Usage
You need a KTextEditor-based text editor with third party plugins support to use this software. [Kate](https://kate-editor.org/) is a good option. You also need a working LanguageTool server (or another spell/grammar checking server compatible with [LanguageTool's HTTP/JSON API](https://languagetool.org/http-api/swagger-ui/)). You can run such a server locally following these [instructions](http://wiki.languagetool.org/http-server). Using LanguageTooler GmbH's servers without permission is not approved.

# License
This project is placed under protection of the GNU General Public License version 3 or later as published by the Free Software Foundation.

## Contributions
Contributions are welcome. By contributing to this project, you accept that your contribution is GPLv3+ and can be relicensed to GNU Lesser General Public License version 2.0 or later.

# Support
This project is in (early) **beta** state.

This project is currently maintained by a single developer on free time with limited testing possibilities; do not expect professional-grade support. You can fill issues but there is no warranty that they will be addressed. Please try to make reports as good as possible (information to reproduce is mandatory, providing a tested fix is great).

This project is mainly tested on Gentoo ~amd64 and may **not** work properly on systems without recent versions of its dependencies.

All recent POSIX compliant systems should be supported with reasonable adaptations (things a distribution packager can do). Non-POSIX-compliant systems, including MS Windows, are **not** officially supported. Non-FOSS POSIX-compliant systems, including macOS, will **not** receive any special attention a GNU/Linux or *BSD system could expect (no fix for macOS-only issues).

This project is intended to be built with GCC (and libstdc++). Issues encountered with Clang (and libc++) can be considered as *should fix*. Other compilers (and std) are **unsupported**.

# Affiliation
This project is not affiliated to nor endorsed by LanguageTooler GmbH and using it with LanguageTooler GmbH's servers without permission is not approved. This project currently has no link with KDE e.V.

# Code of Conduct
Please follow the [KDE Community Code of Conduct](https://kde.org/code-of-conduct/)
