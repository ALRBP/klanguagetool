#include <vector>
#include <unordered_map>
#include <algorithm>
#include <fstream>
#include <filesystem>
#include <KLocalizedString>
#include <KPluginFactory>
#include <KPluginLoader>
#include <KConfig>
#include <KSharedConfig>
#include <KConfigGroup>
#include "config/definitions.h++"
#include "tools/ltjson.h++"
#include "klanguagetoolview.h++"
#include "klanguagetoolconfigwidget.h++"
#include "klanguagetool.h++"

#define CONFIG_PAGES 1


K_PLUGIN_FACTORY_WITH_JSON(KLanguageToolFactory,"klanguagetool.json",registerPlugin<KLanguageTool>();)


KLanguageTool::KLanguageTool(QObject* parent,[[maybe_unused]] const QVariantList &args): KTextEditor::Plugin(parent)
{
    m_views = QSet<KLanguageToolView*>();
    manager = (new QNetworkAccessManager());
    std::ifstream f(std::filesystem::path(CONFIG_PATH)/CONFIG,std::ifstream::in);
    std::string s;
    std::getline(f,s,'\0');
    f.close();
    defaults = getDefaults(s);
    f.open(std::filesystem::path(CONFIG_PATH)/FORMATS,std::ifstream::in);
    std::getline(f,s,'\0');
    f.close();
    formats = getFormats(s,defaults.delimiters);
    std::unordered_map<std::string,KLTFormat> formatsSave = formats;
    try
    {
        f.open(std::filesystem::path(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation).toStdString())/defaults.id/FORMATS,std::ifstream::in);
        if(f.is_open())
        {
            std::getline(f,s,'\0');
            getFormats(formats,s,defaults.delimiters);
        }
    }
    catch(...)
    {
        formats = formatsSave;
    }
    if(f.is_open())
    {
        f.close();
    }
    std::unordered_map<std::string,std::string> defMap;
    for(const auto& l: formats)
    {
        if(l.second.marxon)
        {
            f.open(std::filesystem::path(Marxon::dataPath())/(l.second.marxonName+".yaml"),std::ifstream::in);
            if(f.is_open())
            {
                std::getline(f,s,'\0');
                defMap[l.second.marxonName] = s;
                f.close();
            }
            f.open(std::filesystem::path(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation).toStdString())/defaults.marxonFolder/(l.second.marxonName+".yaml"),std::ifstream::in);
            if(f.is_open())
            {
                std::getline(f,s,'\0');
                defMap[l.second.marxonName] = s;
                f.close();
            }
        }
    }
    slotResetConfig();
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    if(!config.hasKey("dictionary"))
    {
        config.writeEntry("dictionary",QStringList());
    }
    QStringList dictmp = config.readEntry("dictionary",QStringList());
    for(const auto& w: dictmp)
    {
        dict.insert(w.toStdString());
    }
    try
    {
        marxonDefs = Marxon::Definitions(defMap);
    }
    catch(...)
    {
        marxonDefs = Marxon::Definitions();
    }
}


KLanguageTool::~KLanguageTool(void)
{
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    QStringList dico;
    for(const auto& w: dict)
    {
        dico.push_back(QString::fromStdString(w));
    }
    config.writeEntry("dictionary",dico);
}
    
QObject* KLanguageTool::createView(KTextEditor::MainWindow* mw)
{
    return new KLanguageToolView(this,mw);
}
    
int KLanguageTool::configPages(void) const
{
    return CONFIG_PAGES;
}

KTextEditor::ConfigPage* KLanguageTool::configPage(int number, QWidget* parent)
{
    if(number == 0)
    {         
        return new KLanguageToolConfigWidget(parent,this);
    }
    return nullptr;
}

void KLanguageTool::slotServParams(QNetworkReply* reply)
{
    active = false;
    if(reply != nullptr)
    {
        QByteArray data = reply->readAll();
        reply->deleteLater();
        std::string jsons = data.toStdString();
        supportedLangs.clear();
        invertLangs.clear();
        try
        {
            nlohmann::json strj = nlohmann::json::parse(jsons);
            for(const auto& l: strj)
            {
                std::string name, code;
                l.at("name").get_to(name);
                l.at("longCode").get_to(code);
                supportedLangs[i18n(name.c_str()).toStdString()] = code;
                invertLangs[code] = i18n(name.c_str()).toStdString();
            }
        }
        catch(...){}
    }
    std::vector<std::string> sortedLangs;
    for(const auto& l: supportedLangs)
    {
        sortedLangs.push_back(l.first);
    }
    std::stable_sort(sortedLangs.begin(),sortedLangs.end());
    for(int i = 0; i < useLang.size(); ++i)
    {
        if(useLang[i] == '_')
        {
            useLang[i] = '-';
        }
    }
    for(int i = 0; i < useNativeLang.size(); ++i)
    {
        if(useNativeLang[i] == '_')
        {
            useNativeLang[i] = '-';
        }
    }
    if(invertLangs.find(useLang.toStdString()) == invertLangs.end())
    {
        useLang = QString::fromStdString(std::string(useLang.toStdString(),0,useLang.toStdString().find('-')));
        if(invertLangs.find(useLang.toStdString()) == invertLangs.end())
        {
            useLang = KConfig().locale();
            for(int i = 0; i < useLang.size(); ++i)
            {
                if(useLang[i] == '_')
                {
                    useLang[i] = '-';
                }
            }
            if(invertLangs.find(useLang.toStdString()) == invertLangs.end())
            {
                useLang = QString::fromStdString(std::string(useLang.toStdString(),0,useLang.toStdString().find('-')));
                if(invertLangs.find(useLang.toStdString()) == invertLangs.end())
                {
                    if(invertLangs.empty())
                    {
                        supportedLangs[i18n(defaults.noLang.c_str()).toStdString()] = i18n(defaults.noLang.c_str()).toStdString();
                        invertLangs[i18n(defaults.noLang.c_str()).toStdString()] = i18n(defaults.noLang.c_str()).toStdString();
                        lang = QString::fromStdString(defaults.noLang);
                        useLang = QString::fromStdString(defaults.noLang);
                    }
                    else
                    {
                        useLang = QString::fromStdString(invertLangs.begin()->first);
                    }
                }
            }
            lang = useLang;
        }
    }
    if(invertLangs.find(useNativeLang.toStdString()) == invertLangs.end())
    {
        useNativeLang = QString::fromStdString(std::string(useNativeLang.toStdString(),0,useNativeLang.toStdString().find('-')));
        if(invertLangs.find(useNativeLang.toStdString()) == invertLangs.end())
        {
            useNativeLang = KConfig().locale();
            for(int i = 0; i < useNativeLang.size(); ++i)
            {
                if(useNativeLang[i] == '_')
                {
                    useNativeLang[i] = '-';
                }
            }
            if(invertLangs.find(useNativeLang.toStdString()) == invertLangs.end())
            {
                useNativeLang = QString::fromStdString(std::string(useNativeLang.toStdString(),0,useNativeLang.toStdString().find('-')));
                if(invertLangs.find(useNativeLang.toStdString()) == invertLangs.end())
                {
                    if(invertLangs.empty())
                    {
                        supportedLangs[i18n(defaults.noLang.c_str()).toStdString()] = i18n(defaults.noLang.c_str()).toStdString();
                        invertLangs[i18n(defaults.noLang.c_str()).toStdString()] = i18n(defaults.noLang.c_str()).toStdString();
                        nativeLang = QString::fromStdString(defaults.noLang);
                        useNativeLang = QString::fromStdString(defaults.noLang);
                    }
                    else
                    {
                        useNativeLang = QString::fromStdString(invertLangs.begin()->first);
                    }
                }
            }
            nativeLang = useNativeLang;
        }
    }
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    config.writeEntry("language",lang);
    config.writeEntry("native_language",nativeLang);
    active = true;
    if(reply != nullptr)
    {
        emit signalServReply();
        emit signalParamsChange();
    }
}

void KLanguageTool::slotResetConfig(void)
{
    active = false;
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    if(!config.hasKey("on"))
    {
        config.writeEntry("on",defaults.on);
    }
    on = config.readEntry("on",defaults.on);
    if(!config.hasKey("auto_on"))
    {
        config.writeEntry("auto_on",defaults.on);
    }
    onAuto = config.readEntry("auto_on",defaults.on);
    if(!config.hasKey("marxon"))
    {
        config.writeEntry("marxon",defaults.marxon);
    }
    marxon = config.readEntry("marxon",defaults.marxon);
    if(!config.hasKey("auto_marxon"))
    {
        config.writeEntry("auto_marxon",defaults.marxon);
    }
    marxonAuto = config.readEntry("auto_marxon",defaults.marxon);
    if(!config.hasKey("server"))
    {
        config.writeEntry("server",QString::fromStdString(defaults.server));
    }
    serv = config.readEntry("server",QString::fromStdString(defaults.server));
    if(!config.hasKey("low_color"))
    {
        config.writeEntry("low_color",defaults.lowColor);
    }
    lowColor = config.readEntry("low_color",defaults.lowColor);
    if(!config.hasKey("mid_color"))
    {
        config.writeEntry("mid_color",defaults.midColor);
    }
    midColor = config.readEntry("mid_color",defaults.midColor);
    if(!config.hasKey("high_color"))
    {
        config.writeEntry("high_color",defaults.highColor);
    }
    highColor = config.readEntry("high_color",defaults.highColor);
    if(!config.hasKey("higher_color"))
    {
        config.writeEntry("higher_color",defaults.higherColor);
    }
    higherColor = config.readEntry("higher_color",defaults.higherColor);
    if(!config.hasKey("misc_color"))
    {
        config.writeEntry("misc_color",defaults.miscColor);
    }
    miscColor = config.readEntry("misc_color",defaults.miscColor);
    if(!config.hasKey("throttle"))
    {
        config.writeEntry("throttle",defaults.throttle);
    }
    throttle = std::chrono::milliseconds(config.readEntry("throttle",defaults.throttle));
    if(!config.hasKey("language"))
    {
        config.writeEntry("language",KConfig().locale());
    }
    lang = config.readEntry("language",KConfig().locale());
    if(!config.hasKey("native_language"))
    {
        config.writeEntry("native_language",KConfig().locale());
    }
    nativeLang = config.readEntry("native_language",KConfig().locale());
    if(!config.hasKey("max_suggestions"))
    {
        config.writeEntry("max_suggestions",defaults.sugMax);
    }
    sugMax = config.readEntry("max_suggestions",defaults.sugMax);
    if(!config.hasKey("max_links"))
    {
        config.writeEntry("max_links",defaults.linkMax);
    }
    linkMax = config.readEntry("max_links",defaults.linkMax);
    if(!config.hasKey("net_timeout"))
    {
        config.writeEntry("net_timeout",defaults.netTimeout);
    }
    netTimeout = config.readEntry("net_timeout",defaults.netTimeout);
    useLang = lang;
    useNativeLang = nativeLang;
    QNetworkRequest req(QUrl(serv+QString::fromStdString(defaults.langsPath)));
    req.setTransferTimeout(netTimeout);
    req.setRawHeader(QByteArray::fromStdString("Accept"),QByteArray::fromStdString("application/json"));
    QNetworkReply* reply = manager->get(req);
    connect(reply,&QNetworkReply::finished,[this,reply](){slotServParams(reply);});
}

void KLanguageTool::slotAddDict(const std::string& word)
{
    dict.insert(word);
    emit signalParamsChange();
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    QStringList dico;
    for(const auto& w: dict)
    {
        dico.push_back(QString::fromStdString(w));
    }
    config.writeEntry("dictionary",dico);
}

void KLanguageTool::slotRemDict(const std::string& word)
{
    dict.erase(word);
    emit signalParamsChange();
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    QStringList dico;
    for(const auto& w: dict)
    {
        dico.push_back(QString::fromStdString(w));
    }
    config.writeEntry("dictionary",dico);
}

void KLanguageTool::slotClearDict(void)
{
    dict.clear();
    emit signalParamsChange();
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(defaults.config));
    QStringList dico;
    for(const auto& w: dict)
    {
        dico.push_back(QString::fromStdString(w));
    }
    config.writeEntry("dictionary",dico);
}

void KLanguageTool::slotServSync(void)
{
    QNetworkRequest req(QUrl(serv+QString::fromStdString(defaults.langsPath)));
    req.setTransferTimeout(netTimeout);
    req.setRawHeader(QByteArray::fromStdString("Accept"),QByteArray::fromStdString("application/json"));
    QNetworkReply* reply = manager->get(req);
    connect(reply,&QNetworkReply::finished,[this,reply](){slotServParams(reply);});
}


#include "klanguagetool.moc"
