#include <QtCore/QUrl>
#include <QtCore/QTimer>
#include <QtGui/QDesktopServices>
#include <QtWidgets/QMenu>
#include <KTextEditor/Attribute>
#include <KTextEditor/ConfigInterface>
#include <KTextEditor/Cursor>
#include <KTextEditor/Document>//TODO? suggest KTE devs to add locale and locale-range requests in public API?
#include <KTextEditor/MovingCursor>
#include <KTextEditor/MovingInterface>
#include <KTextEditor/View>
#include <KLocalizedString>
#include <KActionCollection>
#include <KConfig>
#include <KSharedConfig>
#include <KConfigGroup>
#include <KXMLGUIFactory>
#include "klanguagetoolview.h++"


KLanguageToolView::KLanguageToolView(KLanguageTool* plugin, KTextEditor::MainWindow* mw)
{
    m_mainWindow = mw;
    m_plugin = plugin;
    KXMLGUIClient::setComponentName(QString::fromStdString(m_plugin->defaults.id),i18n(m_plugin->defaults.name.c_str()));
    setXMLFile(QStringLiteral("ui.rc"));
    lastCheckTime = std::chrono::steady_clock::time_point(std::chrono::steady_clock::duration::zero());
    waitingToCheck = false;
    checkCanceled = false;
    fullCheck = false;
    waitingForReplies = 0;
    blocks = std::list<KateBlock>();
    tmpBlocks = std::list<KateBlock>();
    matches = std::vector<KateMatch>();
    tmpMatches = std::vector<KateMatch>();
    ignore = std::unordered_set<std::string>();
    connect(m_mainWindow,&KTextEditor::MainWindow::viewChanged,this,&KLanguageToolView::slotDocChanged);
    connect(m_plugin,&KLanguageTool::signalParamsChange,this,&KLanguageToolView::slotFullCheck);
    connect(m_plugin,&KLanguageTool::signalParamsChange,this,&KLanguageToolView::slotUpdateToolMenu);
    slotDocChanged();
    m_plugin->m_views.insert(this);
    m_popup = new KActionMenu(m_plugin->defaults.icon,i18n(m_plugin->defaults.name.c_str()),this);
    actionCollection()->addAction(QStringLiteral("popup_klanguagetool"),m_popup);
    m_popup->setEnabled(false);
    m_tools = new KActionMenu(m_plugin->defaults.icon,i18n(m_plugin->defaults.name.c_str()),this);
    actionCollection()->addAction(QStringLiteral("tools_klanguagetool"),m_tools);
    slotUpdateToolMenu();
    m_tools->setEnabled(true);
    m_mainWindow->guiFactory()->addClient(this);
}

KLanguageToolView::~KLanguageToolView(void)
{
    for(const auto& c: docCon)
    {
        disconnect(*c);
    }
    m_mainWindow->guiFactory()->removeClient(this);
    m_plugin->m_views.remove(this);
}

void KLanguageToolView::slotDocChanged(void)
{
    for(const auto& c: docCon)
    {
        disconnect(*c);
    }
    //TODO Find a way to clear/manage ranges properly
    blocks.clear();
    matches.clear();
    tmpBlocks.clear();
    tmpMatches.clear();
    docCon.clear();
    ignore.clear();
    fullCheck = false;
    if((m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr))
    {
        docCon+= (new QMetaObject::Connection(connect(m_mainWindow->activeView(),&KTextEditor::View::contextMenuAboutToShow,this,&KLanguageToolView::slotContextMenuAboutToShow)));
        lastCheckTime = std::chrono::steady_clock::time_point(std::chrono::steady_clock::duration::zero());
        docCon+= (new QMetaObject::Connection(connect(m_mainWindow->activeView()->document(),&KTextEditor::Document::textChanged,this,&KLanguageToolView::slotNeedCheck)));   
        docCon+= (new QMetaObject::Connection(connect(m_mainWindow->activeView()->document(),&KTextEditor::Document::highlightingModeChanged,this,&KLanguageToolView::slotFullCheck)));
        slotFullCheck();
    }
    else
    {
        if(waitingToCheck)
        {
            checkCanceled = true;
        }
    }
}

void KLanguageToolView::slotNeedCheck(void)
{
    if((m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr) && (!waitingToCheck) && (m_plugin->on) && (m_plugin->active) && ((!m_plugin->onAuto) || ((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) == m_plugin->formats.end()) && (m_plugin->defaults.onDefault)) || ((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) != m_plugin->formats.end()) && (m_plugin->formats[m_mainWindow->activeView()->document()->mimeType().toStdString()].on))))
    {
        waitingToCheck = true;
        checkCanceled = false;
        int time = (std::chrono::duration_cast<std::chrono::milliseconds>(lastCheckTime+std::chrono::milliseconds(m_plugin->throttle)-std::chrono::steady_clock::now())).count();
        if(time > 0)
        {
            QTimer::singleShot(time,this,&KLanguageToolView::slotRefreshCheck);
        }
        else
        {
            slotRefreshCheck();
        }
    }
}

void KLanguageToolView::slotFullCheck(void)
{
    fullCheck = true;
    slotNeedCheck();
}

void KLanguageToolView::slotRefreshCheck(void)
{
    if(checkCanceled || (!m_plugin->on) || (!m_plugin->active) || (m_mainWindow->activeView() == nullptr) || (m_mainWindow->activeView()->document() == nullptr) || !((!m_plugin->onAuto) || ((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) == m_plugin->formats.end()) && (m_plugin->defaults.onDefault)) || ((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) != m_plugin->formats.end()) && (m_plugin->formats[m_mainWindow->activeView()->document()->mimeType().toStdString()].on))))
    {
        waitingToCheck = false;
        fullCheck = false;
        return;
    }
    std::list<std::pair<KateBlock,std::pair<QByteArray,std::vector<KTextEditor::MovingCursor*>>>> parts;
    std::string mode = m_mainWindow->activeView()->document()->mimeType().toStdString(), umode;
    if(m_plugin->formats.find(mode) != m_plugin->formats.end())
    {
        umode = m_plugin->formats[mode].marxonName;
    }
    else
    {
        umode = mode;
    }
    KTextEditor::MovingInterface* moving = qobject_cast<KTextEditor::MovingInterface*>(m_mainWindow->activeView()->document());
    if(m_plugin->marxon && ((!m_plugin->marxonAuto) || ((m_plugin->formats.find(mode) == m_plugin->formats.end()) && (m_plugin->defaults.marxonDefault)) || ((m_plugin->formats.find(mode) != m_plugin->formats.end()) && (m_plugin->formats[mode].marxon))) && m_plugin->marxonDefs.isSupported(umode))
    {
        try
        {
            Marxon::Text text = m_plugin->marxonDefs.jsonList(m_mainWindow->activeView()->document()->text().toStdString(),umode);
            for(const auto& p: text)
            {
                if(p.hasText)
                {
                    KTextEditor::MovingCursor* beg = moving->newMovingCursor(KTextEditor::Cursor(0,0));
                    KTextEditor::MovingCursor* end = moving->newMovingCursor(KTextEditor::Cursor(0,0));
                    beg->move(p.offset16);
                    end->move(p.offset16+p.len16);
                    bool changed = true;
                    KateBlock block;
                    if(!fullCheck)
                    {
                        for(const auto& b: blocks)
                        {
                            if((b.range->start() == *beg) && (b.range->end() == *end) && (b.text == m_mainWindow->activeView()->document()->text(*(b.range))))
                            {
                                changed = false;
                                block = b;
                            }
                        }
                    }
                    if(changed)
                    {
                        std::vector<KTextEditor::MovingCursor*> curs;
                        curs.reserve(p.len16+1);
                        for(size_t i = 0; i <= p.len16; ++i)
                        {
                            curs.push_back(moving->newMovingCursor(KTextEditor::Cursor(0,0)));
                            curs[i]->move(p.offset16+i);
                        }
                        block.range = moving->newMovingRange(KTextEditor::Range(*beg,*end));
                        block.text = m_mainWindow->activeView()->document()->text(*(block.range));
                        parts.push_back(std::pair<KateBlock,std::pair<QByteArray,std::vector<KTextEditor::MovingCursor*>>>(block,std::pair<QByteArray,std::vector<KTextEditor::MovingCursor*>>(QByteArray("data=")+QUrl::toPercentEncoding(QString::fromStdString("{\"annotation\":"+p.text+"}"))+QByteArray::fromStdString("&language="+m_plugin->useLang.toStdString()+"&motherTongue="+m_plugin->useNativeLang.toStdString()+"&enabledOnly=false"),curs)));
                    }
                    else
                    {
                        std::list<size_t> tm;
                        tmpMatches.reserve(tmpMatches.size()+block.matches.size());
                        for(const auto& m: block.matches)
                        {
                            tm.push_back(tmpMatches.size());
                            tmpMatches.push_back(matches[m]);
                            tmpMatches.back().range = moving->newMovingRange(KTextEditor::Range(tmpMatches.back().range->start(),tmpMatches.back().range->end()));
                        }
                        block.matches = tm;
                        tmpBlocks.push_back(block);
                    }
                }
            }
        }
        catch(...)
        {
            goto unsupported;
        }
    }
    else
    {
        unsupported:
        std::list<std::pair<KTextEditor::MovingRange*,size_t>> ranges;
        bool toCheck = false;
        KTextEditor::MovingCursor* mem = nullptr;
        KTextEditor::MovingCursor* i = moving->newMovingCursor(KTextEditor::Cursor(0,0));
        std::vector<QList<KTextEditor::AttributeBlock>> linesAttributes;
        size_t lines = m_mainWindow->activeView()->document()->lines();
        linesAttributes.reserve(lines);
        for(size_t i = 0; i < lines; ++i)
        {
            linesAttributes.push_back(m_mainWindow->activeView()->lineAttributes(i));
        }
        size_t len = 0;
        do
        {
            if(!i->isValidTextPosition())
            {
                i->move(1);
                if(!i->isValidTextPosition())
                {
                    break;
                }
            }
            bool invert = ((m_plugin->formats.find(mode) == m_plugin->formats.end()) && (m_plugin->defaults.invert)) || ((m_plugin->formats.find(mode) != m_plugin->formats.end()) && (m_plugin->formats[mode].invert));
            bool checkChar = invert;
            for(const auto& b: linesAttributes[i->line()])
            {
                if((b.start <= i->column()) && (i->column() < (b.start+b.length)))
                {
                    if(b.attribute->skipSpellChecking())//TODO BUG Discus strange behavior with KTE devs
                    {
                        checkChar = !invert;
                        break;
                    }
                }
            }
            if(checkChar)
            {
                if(!toCheck)
                {
                    mem = moving->newMovingCursor(*i);
                    toCheck = true;
                    len = 2;
                }
                else
                {
                    ++len;
                }
            }
            else
            {
                if(toCheck)
                {
                    ranges.push_back(std::pair<KTextEditor::MovingRange*,size_t>(moving->newMovingRange(KTextEditor::Range(*mem,*i)),len));
                    toCheck = false;
                }
            }
        }
        while(i->move(1));
        if(toCheck)
        {
            ranges.push_back(std::pair<KTextEditor::MovingRange*,size_t>(moving->newMovingRange(KTextEditor::Range(*mem,*i)),len));
        }
        for(const auto& r: ranges)
        {
            bool changed = true;
            KateBlock block;
            if(!fullCheck)
            {
                for(const auto& b: blocks)
                {
                    if((b.range->start() == r.first->start()) && (b.range->end() == r.first->end()) && (b.text == m_mainWindow->activeView()->document()->text(*(b.range))))
                    {
                        changed = false;
                        block = b;
                    }
                }
            }
            if(changed)
            {
                QString txt = m_mainWindow->activeView()->document()->text(*(r.first));
                if((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) != m_plugin->formats.end()))
                {
                    for(const auto& d: m_plugin->formats[m_mainWindow->activeView()->document()->mimeType().toStdString()].delimiters)
                    {
                        if((((size_t)txt.size()) >= d.first.size()) && (((size_t)txt.size()) >= d.second.size()) && txt.startsWith(QString::fromStdString(d.first)) && txt.endsWith(QString::fromStdString(d.second)))
                        {
                            txt.remove(0,d.first.size());
                            txt.chop(d.second.size());
                            break;
                        }
                    }
                }
                if(txt.size() > 0)
                {
                    std::vector<KTextEditor::MovingCursor*> curs;
                    curs.reserve(r.second);
                    for(size_t i = 0; ((i == 0) || (*(curs[i-1]) < r.first->end())); ++i)
                    {
                        curs.push_back(moving->newMovingCursor(r.first->start()));
                        curs[i]->move(i);
                    }
                    block.range = moving->newMovingRange(KTextEditor::Range(r.first->start(),r.first->end()));
                    block.text = m_mainWindow->activeView()->document()->text(*(block.range));
                    parts.push_back(std::pair<KateBlock,std::pair<QByteArray,std::vector<KTextEditor::MovingCursor*>>>(block,std::pair<QByteArray,std::vector<KTextEditor::MovingCursor*>>(QByteArray("text=")+QUrl::toPercentEncoding(txt)+QByteArray::fromStdString("&language="+m_plugin->useLang.toStdString()+"&motherTongue="+m_plugin->useNativeLang.toStdString()+"&enabledOnly=false"),curs)));
                }
            }
            else
            {
                std::list<size_t> tm;
                tmpMatches.reserve(tmpMatches.size()+block.matches.size());
                for(const auto& m: block.matches)
                {
                    tm.push_back(tmpMatches.size());
                    tmpMatches.push_back(matches[m]);
                    tmpMatches.back().range = moving->newMovingRange(KTextEditor::Range(tmpMatches.back().range->start(),tmpMatches.back().range->end()));
                }
                block.matches = tm;
                tmpBlocks.push_back(block);
            }
        }
    }
    waitingForReplies = parts.size();
    if(waitingForReplies > 0)
    {
        QNetworkRequest req(QUrl(m_plugin->serv+QString::fromStdString(m_plugin->defaults.checkPath)));
        req.setTransferTimeout(m_plugin->netTimeout);
        req.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/x-www-form-urlencoded"));
        req.setRawHeader(QByteArray::fromStdString("Accept"),QByteArray::fromStdString("application/json"));
        for(const auto& p: parts)
        {
            QNetworkReply* reply = m_plugin->manager->post(req,p.second.first);
            connect(reply,&QNetworkReply::finished,[this,reply,conv=p.second.second,b=p.first](){slotCheckReplied(reply,conv,b);});
        }
    }
    else
    {
        for(const auto& m: matches)
        {
            m.range->setAttribute(KTextEditor::Attribute::Ptr(nullptr));
        }
        blocks = tmpBlocks;
        matches = tmpMatches;
        tmpBlocks.clear();
        tmpMatches.clear();
        lastCheckTime = std::chrono::steady_clock::now();
        waitingToCheck = false;
        fullCheck = false;
    }
}

void KLanguageToolView::slotCheckReplied(QNetworkReply* reply, const std::vector<KTextEditor::MovingCursor*>& conv, const KateBlock& b1)
{
    KateBlock b = b1;
    QByteArray data = reply->readAll();
    reply->deleteLater();
    if((!checkCanceled) && (m_plugin->on) && (m_plugin->active) && (m_mainWindow->activeView() != nullptr) && (m_mainWindow->activeView()->document() != nullptr) && ((!m_plugin->onAuto) || ((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) == m_plugin->formats.end()) && (m_plugin->defaults.onDefault)) || ((m_plugin->formats.find(m_mainWindow->activeView()->document()->mimeType().toStdString()) != m_plugin->formats.end()) && (m_plugin->formats[m_mainWindow->activeView()->document()->mimeType().toStdString()].on))))
    {
        std::string jsons = data.toStdString();
        std::list<LTMatch> ml;
        try
        {
            nlohmann::json strj = nlohmann::json::parse(jsons);
            if(strj.contains("matches"))
            {
                strj["matches"].get_to(ml);
            }
        }
        catch(...){}
        KTextEditor::MovingInterface* moving = qobject_cast<KTextEditor::MovingInterface*>(m_mainWindow->activeView()->document());
        tmpMatches.reserve(tmpMatches.size()+ml.size());
        for(const auto& m: ml)
        {
            if((m_plugin->dict.find(m_mainWindow->activeView()->document()->text(KTextEditor::Range(*(conv[m.offset]),*(conv[m.offset+m.len]))).toStdString()) == m_plugin->dict.end()) && (ignore.find(m_mainWindow->activeView()->document()->text(KTextEditor::Range(*(conv[m.offset]),*(conv[m.offset+m.len]))).toStdString()) == ignore.end()))
            {
                KateMatch k;
                k.type = m.type;
                k.message = QString::fromStdString(m.message);
                k.shortm = QString::fromStdString(m.shortm);
                k.replace = QList<QPair<QString,QString>>();
                for(const auto& r: m.replace)
                {
                    k.replace.push_back(QPair(QString::fromStdString(r.first),QString::fromStdString(r.first)));
                }
                k.urls = QList<QUrl>();
                for(const auto& u: m.urls)
                {
                    k.urls.push_back(QUrl(QString::fromStdString(u)));
                }
                if(m.len == 0)
                {
                    KTextEditor::MovingCursor* mc = moving->newMovingCursor(*(conv[m.offset]));
                    if(mc->move(1) && (mc->isValidTextPosition() || mc->move(1)))
                    {
                        k.range = moving->newMovingRange(KTextEditor::Range(*(conv[m.offset]),*mc));
                    }
                    else if(((mc->isValidTextPosition() || (mc->move(-1) && (mc->isValidTextPosition() || mc->move(-1)))) && mc->move(-1)) && (mc->isValidTextPosition() || mc->move(-1)))
                    {
                        k.range = moving->newMovingRange(KTextEditor::Range(*mc,*(conv[m.offset+m.len])));
                    }
                    else
                    {
                        k.range = moving->newMovingRange(KTextEditor::Range(*(conv[m.offset]),*(conv[m.offset+m.len])));
                    }
                }
                else
                {
                    k.range = moving->newMovingRange(KTextEditor::Range(*(conv[m.offset]),*(conv[m.offset+m.len])));
                }
                k.repRange = moving->newMovingRange(KTextEditor::Range(*(conv[m.offset]),*(conv[m.offset+m.len])));
                b.matches.push_back(tmpMatches.size());
                tmpMatches.push_back(k);
            }
        }
        tmpBlocks.push_back(b);
    }
    if(--waitingForReplies == 0)
    {
        for(const auto& m: matches)
        {
            m.range->setAttribute(KTextEditor::Attribute::Ptr(nullptr));
        }
        blocks = tmpBlocks;
        matches = tmpMatches;
        tmpBlocks.clear();
        tmpMatches.clear();
        for(const auto& m: matches)
        {
            KTextEditor::Attribute* attribute = new KTextEditor::Attribute();     
            m.range->setAttributeOnlyForViews(true);
            attribute->setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);
            switch(m.type)
            {
                case LTErrorLvl::Low:
                    attribute->setUnderlineColor(m_plugin->lowColor);
                    break;
                case LTErrorLvl::Mid:
                    attribute->setUnderlineColor(m_plugin->midColor);
                    break;
                case LTErrorLvl::High:
                    attribute->setUnderlineColor(m_plugin->highColor);
                    break;
                case LTErrorLvl::Higher:
                    attribute->setUnderlineColor(m_plugin->higherColor);
                    break;
                case LTErrorLvl::Misc:
                    attribute->setUnderlineColor(m_plugin->miscColor);
                    break;
                default:
                    break;
            }
            m.range->setAttribute(KTextEditor::Attribute::Ptr(attribute));
        }
        lastCheckTime = std::chrono::steady_clock::now();
        waitingToCheck = false;
        fullCheck = false;
    }
}

void KLanguageToolView::slotContextMenuAboutToShow(void)
{
    m_popup->menu()->clear();
    m_popup->setEnabled(false);
    KTextEditor::Cursor c = m_mainWindow->activeView()->cursorPosition();
    for(const auto& m: matches)
    {
        if(m.range->contains(c))
        {
            if(m.shortm.size() > 0)
            {
                m_popup->menu()->addAction(i18n("Info: ")+m.shortm);
            }
            if(m.urls.size() > 0)
            {
                size_t count = 0;
                for(const auto& u: m.urls)
                {
                    connect(m_popup->menu()->addAction(i18n("See: ")+u.toDisplayString()),&QAction::triggered,[u](){QDesktopServices::openUrl(u);}); 
                    ++count;
                    if(count >= m_plugin->linkMax)
                    {
                        break;
                    }
                }
            }
            if(m.replace.size() > 0)
            {
                size_t count = 0;
                for(const auto& r: m.replace)
                {
                    connect(m_popup->menu()->addAction(r.first),&QAction::triggered,[this,range=m.repRange,text=r.first](){m_mainWindow->activeView()->document()->replaceText(*range,text);});
                    ++count;
                    if(count >= m_plugin->sugMax)
                    {
                        break;
                    }
                }
            }
            connect(m_popup->menu()->addAction(i18n("Add to local dictionary")),&QAction::triggered,[this,text=m_mainWindow->activeView()->document()->text(*(m.repRange)).toStdString()](){m_plugin->slotAddDict(text);});
            m_popup->setEnabled(true);
            connect(m_popup->menu()->addAction(i18n("Ignore")),&QAction::triggered,[this,text=m_mainWindow->activeView()->document()->text(*(m.repRange)).toStdString()](){slotAddIgnore(text);});
            m_popup->setEnabled(true);
            break;
        }
    }
}

void KLanguageToolView::slotAddIgnore(const std::string& word)
{
    ignore.insert(word);
}

void KLanguageToolView::slotRemIgnore(const std::string& word)
{
    ignore.erase(word);
}

void KLanguageToolView::slotClearIngore(void)
{
    ignore.clear();
}

void KLanguageToolView::slotUpdateToolMenu(void)
{
    m_tools->menu()->clear();
    std::vector<std::string> sortedLangs;
    for(const auto& l: m_plugin->supportedLangs)
    {
        sortedLangs.push_back(l.first);
    }
    std::stable_sort(sortedLangs.begin(),sortedLangs.end());
    QAction* modeActive = new QAction(i18n("Active"),this);
    modeActive->setCheckable(true);
    modeActive->setData('1');
    m_tools->menu()->addAction(modeActive);
    QAction* modeAuto = new QAction(i18n("Auto"),this);
    modeAuto->setCheckable(true);
    modeAuto->setData('a');
    m_tools->menu()->addAction(modeAuto);
    QAction* modeOff = new QAction(i18n("Off"),this);
    modeOff->setCheckable(true);
    modeOff->setData('0');
    m_tools->menu()->addAction(modeOff);
    if(m_plugin->on)
    {
        if(m_plugin->onAuto)
        {
            modeAuto->setChecked(true);
        }
        else
        {
            modeActive->setChecked(true);
        }
    }
    else
    {
        modeOff->setChecked(true);
    }
    modeSel = new QActionGroup(this);
    modeSel->addAction(modeActive);
    modeSel->addAction(modeAuto);
    modeSel->addAction(modeOff);
    QAction* marxonActive = new QAction(i18n("On (Marxon)"),this);
    marxonActive->setCheckable(true);
    marxonActive->setData('1');
    m_tools->menu()->addAction(marxonActive);
    QAction* marxonAuto = new QAction(i18n("Auto (Marxon)"),this);
    marxonAuto->setCheckable(true);
    marxonAuto->setData('a');
    m_tools->menu()->addAction(marxonAuto);
    QAction* marxonOff = new QAction(i18n("Off (Marxon)"),this);
    marxonOff->setCheckable(true);
    marxonOff->setData('0');
    m_tools->menu()->addAction(marxonOff);
    if(m_plugin->marxon)
    {
        if(m_plugin->marxonAuto)
        {
            marxonAuto->setChecked(true);
        }
        else
        {
            marxonActive->setChecked(true);
        }
    }
    else
    {
        marxonOff->setChecked(true);
    }
    marxonSel = new QActionGroup(this);
    marxonSel->addAction(marxonActive);
    marxonSel->addAction(marxonAuto);
    marxonSel->addAction(marxonOff);
    servSync = new QAction(i18n("Sync"),this);
    m_tools->addAction(servSync);
    QMenu* langMenu = m_tools->menu()->addMenu(i18n("Language"));
    QMenu* nativeMenu = m_tools->menu()->addMenu(i18n("Native language"));
    langSel = new QActionGroup(this);
    nativeSel = new QActionGroup(this);
    for(const auto& l: sortedLangs)
    {
        QAction* tmp = new QAction(QString::fromStdString(l), this);
        tmp->setCheckable(true);
        tmp->setData(QString::fromStdString(m_plugin->supportedLangs[l]));
        if(l == m_plugin->invertLangs[m_plugin->lang.toStdString()])
        {
            tmp->setChecked(true);
        }
        langSel->addAction(tmp);
        langMenu->addAction(tmp);
        tmp = new QAction(QString::fromStdString(l), this);
        tmp->setCheckable(true);
        tmp->setData(QString::fromStdString(m_plugin->supportedLangs[l]));
        if(l == m_plugin->invertLangs[m_plugin->nativeLang.toStdString()])
        {
            tmp->setChecked(true);
        }
        nativeSel->addAction(tmp);
        nativeMenu->addAction(tmp);
    }
    m_tools->menu()->addMenu(langMenu);
    m_tools->menu()->addMenu(nativeMenu);
    connect(m_tools->menu()->addAction(i18n("Clear ignore list")),&QAction::triggered,this,&KLanguageToolView::slotClearIngore);
    connect(modeSel,&QActionGroup::triggered,this,&KLanguageToolView::slotModeChange);
    connect(marxonSel,&QActionGroup::triggered,this,&KLanguageToolView::slotMarxonChange);
    connect(servSync,&QAction::triggered,m_plugin,&KLanguageTool::slotServSync);
    connect(langSel,&QActionGroup::triggered,this,&KLanguageToolView::slotLangChange);
    connect(nativeSel,&QActionGroup::triggered,this,&KLanguageToolView::slotNativeLangChange);
}

void KLanguageToolView::slotModeChange(QAction* l)
{
    switch(l->data().toChar().toLatin1())
    {
        case '1':
            m_plugin->on = true;
            m_plugin->onAuto = false;
            break;
        case 'a':
            m_plugin->on = true;
            m_plugin->onAuto = true;
            break;
        case '0':
            m_plugin->on = false;
            break;
    }
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(m_plugin->defaults.config));
    config.writeEntry("on",m_plugin->on);
    config.writeEntry("auto_on",m_plugin->onAuto);
    slotFullCheck();
}

void KLanguageToolView::slotMarxonChange(QAction* l)
{
    switch(l->data().toChar().toLatin1())
    {
        case '1':
            m_plugin->marxon = true;
            m_plugin->marxonAuto = false;
            break;
        case 'a':
            m_plugin->marxon = true;
            m_plugin->marxonAuto = true;
            break;
        case '0':
            m_plugin->marxon = false;
            break;
    }
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(m_plugin->defaults.config));
    config.writeEntry("marxon",m_plugin->marxon);
    config.writeEntry("auto_marxon",m_plugin->marxonAuto);
    slotFullCheck();
}

void KLanguageToolView::slotLangChange(QAction* l)
{
    m_plugin->lang = l->data().toString();
    m_plugin->useLang = l->data().toString();
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(m_plugin->defaults.config));
    config.writeEntry("language",m_plugin->lang);
    slotFullCheck();
}

void KLanguageToolView::slotNativeLangChange(QAction* l)
{
    m_plugin->nativeLang = l->data().toString();
    m_plugin->useNativeLang = l->data().toString();
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(m_plugin->defaults.config));
    config.writeEntry("native_language",m_plugin->nativeLang);
    slotFullCheck();
}
