#ifndef KLANGUAGETOOL_H
#define KLANGUAGETOOL_H
#include <string>
#include <chrono>
#include <unordered_set>
#include <unordered_map>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QList>
#include <QtCore/QSet>
#include <QtGui/QColor>
#include <QWidget>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <KTextEditor/ConfigPage>
#include <KTextEditor/MainWindow>
#include <KTextEditor/Plugin>
#include <marxon.h++>
#include "tools/params.h++"
#include "config/definitions.h++"

#define CONFIG "config.toml"
#define FORMATS "formats.yaml"


class KLanguageToolView;

class KLanguageToolConfigWidget;


class KLanguageTool: public KTextEditor::Plugin
{     
Q_OBJECT
    
    friend class KLanguageToolView;
    
    friend class KLanguageToolConfigWidget;
    
public:
    explicit KLanguageTool(QObject* parent = nullptr, const QList<QVariant>& = QList<QVariant>());
    
    ~KLanguageTool(void) override;
    
    QObject* createView(KTextEditor::MainWindow* mw) override;
    
    int configPages(void) const override;
    
    KTextEditor::ConfigPage* configPage(int number, QWidget* parent) override;
    
public slots:
    void slotServParams(QNetworkReply* reply = nullptr);
    
    void slotResetConfig(void);
    
    void slotAddDict(const std::string& word);
    
    void slotRemDict(const std::string& word);
    
    void slotClearDict(void);

    void slotServSync(void);
    
signals:
    void signalParamsChange(void);
    
    void signalServReply(void);

private:
    QSet<KLanguageToolView*> m_views;
    
    bool on;
    
    bool onAuto;
    
    bool marxon;
    
    bool marxonAuto;
    
    bool active;
    
    QString serv;
    
    QColor lowColor;
    
    QColor midColor;
    
    QColor highColor;
    
    QColor higherColor;
    
    QColor miscColor;
    
    Marxon::Definitions marxonDefs;
    
    std::chrono::milliseconds throttle;
    
    std::unordered_map<std::string,std::string> supportedLangs;
    
    std::unordered_map<std::string,std::string> invertLangs;
    
    QNetworkAccessManager* manager;
    
    QString lang;
    
    QString nativeLang;
    
    QString useLang;
    
    QString useNativeLang;
    
    std::unordered_set<std::string> dict;
    
    size_t sugMax;
    
    size_t linkMax;
    
    int netTimeout;
    
    KLTDefaults defaults;
    
    std::unordered_map<std::string,KLTFormat> formats;
};
#endif
