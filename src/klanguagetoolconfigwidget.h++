#ifndef KLANGUAGETOOL_CONFIG_H
#define KLANGUAGETOOL_CONFIG_H
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QColorDialog>
#include "klanguagetool.h++"


class KLanguageToolConfigWidget: public KTextEditor::ConfigPage
{     
Q_OBJECT

public:
    explicit KLanguageToolConfigWidget(QWidget* parent, KLanguageTool* plugin);
    
    QString name(void) const override;
    
    QString fullName(void) const override;
    
    QIcon icon(void) const override;
    
public slots:
    void apply(void) override;
    
    void reset(void) override;
    
    void defaults(void) override;

    void slotUpdateLangs(void);

    void slotAddDict(void);

    void slotRemDict(void);

    void slotClearDict(void);

    void slotColorDialog(char c);

    void slotColorChanged(char c, const QColor& color);

private:
    KLanguageTool* m_plugin;
    
    QCheckBox* turnOn;
    
    QLineEdit* server;
    
    QPushButton* sync;
    
    QComboBox* langSelect;
    
    QComboBox* nativeLangSelect;
    
    QCheckBox* marxOn;
    
    QComboBox* dictList;
    
    QPushButton* dictAdd;
    
    QPushButton* dictRem;
    
    QPushButton* dictClear;
    
    QPushButton* lowb;
    
    QColorDialog* low;
    
    QPushButton* midb;
    
    QColorDialog* mid;
    
    QPushButton* highb;
    
    QColorDialog* high;
    
    QPushButton* higherb;
    
    QColorDialog* higher;
    
    QPushButton* miscb;
    
    QColorDialog* misc;
    
    QSpinBox* throttle;
    
    QSpinBox* sugMax;
    
    QSpinBox* linkMax;
    
    QSpinBox* netTO;
    
    QPushButton* resetb;
    
    QPushButton* defaultb;
    
    std::vector<std::string> dict;
};
#endif
