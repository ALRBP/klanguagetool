#ifndef KLANGUAGETOOL_UTILS_H
#define KLANGUAGETOOL_UTILS_H

#include <unordered_map>
#include <yaml-cpp/yaml.h>

namespace YAML
{
template<typename K,typename V,typename H,typename P,typename A>//Add support for unordered_map in YAML-cpp (mainlining pending at time of writing)
struct convert<std::unordered_map<K,V,H,P,A>>
{
    static Node encode(const std::unordered_map<K,V,H,P,A>& rhs)
    {
        Node node(NodeType::Map);
        for(const auto& element : rhs)
        {
            node.force_insert(element.first, element.second);
        }
        return node;
    }

    static bool decode(const Node& node, std::unordered_map<K,V,H,P,A>& rhs)
    {
        if(!node.IsMap())
        {
            return false;
        }
        rhs.clear();
        for(const auto& element : node)
        {
            rhs[element.first.as<K>()] = element.second.as<V>();
        }
        return true;
    }
};
}
#endif
