#include <limits>
#include <vector>
#include <algorithm>
#include <QtCore/QUrl>
#include <QtGui/QIcon>
#include <QLabel>
#include <QVBoxLayout>
#include <KConfig>
#include <KSharedConfig>
#include <KConfigGroup>
#include <KLocalizedString>
#include "klanguagetoolconfigwidget.h++"


KLanguageToolConfigWidget::KLanguageToolConfigWidget(QWidget* parent, KLanguageTool* plugin): KTextEditor::ConfigPage(parent)
{
    m_plugin = plugin;
    setWindowTitle(i18n(m_plugin->defaults.settingsName.c_str()));
    setWindowIcon(m_plugin->defaults.icon);
    QVBoxLayout* lo = new QVBoxLayout(this);
    QHBoxLayout* hserv = new QHBoxLayout();
    QHBoxLayout* hlang = new QHBoxLayout();
    QHBoxLayout* hnlang = new QHBoxLayout();
    QHBoxLayout* hdict = new QHBoxLayout();
    QHBoxLayout* hcolor = new QHBoxLayout();
    QHBoxLayout* hnums = new QHBoxLayout();
    QHBoxLayout* hcont = new QHBoxLayout();
    turnOn = new QCheckBox(i18n("Activate"),this);
    turnOn->setTristate();
    QLabel* bLabel = new QLabel(i18n("Server:"),this);
    server = new QLineEdit(m_plugin->serv,this);
    sync = new QPushButton(i18n("Sync"),this);
    QLabel* kLabel = new QLabel(i18n("Language:"),this);
    langSelect = new QComboBox(this);
    QLabel* nLabel = new QLabel(i18n("Native language:"),this);
    nativeLangSelect = new QComboBox(this);
    marxOn = new QCheckBox(i18n("Marxon"),this);
    marxOn->setTristate();
    dictList = new QComboBox(this);
    dictList->setEditable(true);
    dictList->setInsertPolicy(QComboBox::InsertPolicy::InsertAlphabetically);
    dictAdd = new QPushButton(i18n("Add"),this);
    dictRem = new QPushButton(i18n("Remove"),this);
    dictClear = new QPushButton(i18n("Clear"),this);
    lowb = new QPushButton(i18n("Low"),this);
    QPalette pal = lowb->palette();
    pal.setColor(QPalette::Button,m_plugin->lowColor);
    lowb->setAutoFillBackground(true);
    lowb->setPalette(pal);
    lowb->update();
    low = new QColorDialog(m_plugin->lowColor,this);
    midb = new QPushButton(i18n("Mid"),this);
    pal.setColor(QPalette::Button,m_plugin->midColor);
    midb->setPalette(pal);
    midb->update();
    mid = new QColorDialog(m_plugin->midColor,this);
    highb = new QPushButton(i18n("High"),this);
    pal.setColor(QPalette::Button,m_plugin->highColor);
    highb->setPalette(pal);
    highb->update();
    high = new QColorDialog(m_plugin->highColor,this);
    higherb = new QPushButton(i18n("Higher"),this);
    pal.setColor(QPalette::Button,m_plugin->higherColor);
    higherb->setPalette(pal);
    higherb->update();
    higher = new QColorDialog(m_plugin->higherColor,this);
    miscb = new QPushButton(i18n("Other"),this);
    pal.setColor(QPalette::Button,m_plugin->miscColor);
    miscb->setPalette(pal);
    miscb->update();
    misc = new QColorDialog(m_plugin->miscColor,this);
    QLabel* tLabel = new QLabel(i18n("Check throttle:"),this);
    throttle = new QSpinBox(this);
    QLabel* sLabel = new QLabel(i18n("Max suggestions:"),this);
    sugMax = new QSpinBox(this);
    QLabel* lLabel = new QLabel(i18n("Max links:"),this);
    linkMax = new QSpinBox(this);
    QLabel* oLabel = new QLabel(i18n("Net timeout:"),this);
    netTO = new QSpinBox(this);
    resetb = new QPushButton(i18n("Reset"),this);
    defaultb = new QPushButton(i18n("Default"),this);
    turnOn->setCheckState((m_plugin->on)?((m_plugin->onAuto)?(Qt::PartiallyChecked):(Qt::Checked)):(Qt::Unchecked));
    marxOn->setCheckState((m_plugin->marxon)?((m_plugin->marxonAuto)?(Qt::PartiallyChecked):(Qt::Checked)):(Qt::Unchecked));
    throttle->setMinimum(0);
    throttle->setMaximum(m_plugin->defaults.maxInt);
    sugMax->setMinimum(0);
    sugMax->setMaximum(m_plugin->defaults.maxInt);
    linkMax->setMinimum(0);
    linkMax->setMaximum(m_plugin->defaults.maxInt);
    netTO->setMinimum(0);
    netTO->setMaximum(m_plugin->defaults.maxInt);
    throttle->setValue(m_plugin->throttle.count());
    sugMax->setValue(m_plugin->sugMax);
    linkMax->setValue(m_plugin->linkMax);
    netTO->setValue(m_plugin->netTimeout);
    hserv->addWidget(bLabel);
    hserv->addWidget(server);
    hserv->addWidget(sync);
    hlang->addWidget(kLabel);
    hlang->addWidget(langSelect);
    hnlang->addWidget(nLabel);
    hnlang->addWidget(nativeLangSelect);
    hdict->addWidget(dictList);
    hdict->addWidget(dictAdd);
    hdict->addWidget(dictRem);
    hdict->addWidget(dictClear);
    hcolor->addWidget(lowb);
    hcolor->addWidget(midb);
    hcolor->addWidget(highb);
    hcolor->addWidget(higherb);
    hcolor->addWidget(miscb);
    hnums->addWidget(tLabel);
    hnums->addWidget(throttle);
    hnums->addWidget(sLabel);
    hnums->addWidget(sugMax);
    hnums->addWidget(lLabel);
    hnums->addWidget(linkMax);
    hnums->addWidget(oLabel);
    hnums->addWidget(netTO);
    hcont->addWidget(resetb);
    hcont->addWidget(defaultb);
    lo->addWidget(turnOn);
    lo->addLayout(hserv);
    lo->addLayout(hlang);
    lo->addLayout(hnlang);
    lo->addWidget(marxOn);
    lo->addLayout(hdict);
    lo->addLayout(hcolor);
    lo->addLayout(hnums);
    lo->addLayout(hcont);
    for(const auto& w: m_plugin->dict)
    {
        dict.push_back(w);
    }
    std::stable_sort(dict.begin(),dict.end());
    for(const auto& w: dict)
    {
        dictList->addItem(QString::fromStdString(w));
    }
    slotUpdateLangs();
    connect(m_plugin,&KLanguageTool::signalServReply,this,&KLanguageToolConfigWidget::slotUpdateLangs);
    connect(resetb,&QPushButton::clicked,this,&KLanguageToolConfigWidget::reset);
    connect(defaultb,&QPushButton::clicked,this,&KLanguageToolConfigWidget::defaults);
    connect(dictAdd,&QPushButton::clicked,this,&KLanguageToolConfigWidget::slotAddDict);
    connect(dictRem,&QPushButton::clicked,this,&KLanguageToolConfigWidget::slotRemDict);
    connect(dictClear,&QPushButton::clicked,this,&KLanguageToolConfigWidget::slotClearDict);
    connect(sync,&QPushButton::clicked,m_plugin,&KLanguageTool::slotServSync);
    connect(lowb,&QPushButton::clicked,this,[this](){slotColorDialog('l');});
    connect(midb,&QPushButton::clicked,this,[this](){slotColorDialog('m');});
    connect(highb,&QPushButton::clicked,this,[this](){slotColorDialog('h');});
    connect(higherb,&QPushButton::clicked,this,[this](){slotColorDialog('i');});
    connect(miscb,&QPushButton::clicked,this,[this](){slotColorDialog('o');});
    connect(low,&QColorDialog::colorSelected,this,[this](const QColor& color){slotColorChanged('l',color);});
    connect(mid,&QColorDialog::colorSelected,this,[this](const QColor& color){slotColorChanged('m',color);});
    connect(high,&QColorDialog::colorSelected,this,[this](const QColor& color){slotColorChanged('h',color);});
    connect(higher,&QColorDialog::colorSelected,this,[this](const QColor& color){slotColorChanged('i',color);});
    connect(misc,&QColorDialog::colorSelected,this,[this](const QColor& color){slotColorChanged('o',color);});
    connect(turnOn,&QCheckBox::stateChanged,this,&KLanguageToolConfigWidget::changed);
    connect(marxOn,&QCheckBox::stateChanged,this,&KLanguageToolConfigWidget::changed);
    connect(server,&QLineEdit::textChanged,this,&KLanguageToolConfigWidget::changed);
    connect(langSelect,&QComboBox::currentTextChanged,this,&KLanguageToolConfigWidget::changed);
    connect(nativeLangSelect,&QComboBox::currentTextChanged,this,&KLanguageToolConfigWidget::changed);
    connect(throttle,QOverload<int>::of(&QSpinBox::valueChanged),this,&KLanguageToolConfigWidget::changed);
    connect(sugMax,QOverload<int>::of(&QSpinBox::valueChanged),this,&KLanguageToolConfigWidget::changed);
    connect(linkMax,QOverload<int>::of(&QSpinBox::valueChanged),this,&KLanguageToolConfigWidget::changed);
    connect(netTO,QOverload<int>::of(&QSpinBox::valueChanged),this,&KLanguageToolConfigWidget::changed);
}

QString KLanguageToolConfigWidget::name(void) const
{
    return i18n(m_plugin->defaults.name.c_str());
}

QString KLanguageToolConfigWidget::fullName(void) const
{
    return i18n(m_plugin->defaults.settingsName.c_str());
}

QIcon KLanguageToolConfigWidget::icon(void) const
{
    return m_plugin->defaults.icon;
}

void KLanguageToolConfigWidget::apply(void)
{
    m_plugin->on = (turnOn->checkState() != Qt::Unchecked);
    m_plugin->onAuto = (turnOn->checkState() == Qt::PartiallyChecked);
    m_plugin->marxon = (marxOn->checkState() != Qt::Unchecked);
    m_plugin->marxonAuto = (marxOn->checkState() == Qt::PartiallyChecked);
    m_plugin->serv = server->text();
    m_plugin->throttle = std::chrono::milliseconds(throttle->value());
    m_plugin->lowColor = low->currentColor();
    m_plugin->midColor = mid->currentColor();
    m_plugin->highColor = high->currentColor();
    m_plugin->higherColor = higher->currentColor();
    m_plugin->miscColor = misc->currentColor();
    m_plugin->sugMax = sugMax->value();
    m_plugin->linkMax = linkMax->value();
    m_plugin->netTimeout = netTO->value();
    QString tmp = QString::fromStdString(m_plugin->supportedLangs[langSelect->currentText().toStdString()]);
    if(tmp != m_plugin->useLang)
    {
        m_plugin->lang = tmp;
        m_plugin->useLang = tmp;
    }
    tmp = QString::fromStdString(m_plugin->supportedLangs[nativeLangSelect->currentText().toStdString()]);
    if(tmp != m_plugin->useNativeLang)
    {
        m_plugin->nativeLang = tmp;
        m_plugin->useNativeLang = tmp;
    }
    m_plugin->slotServParams();
    m_plugin->dict.clear();
    QStringList dico;
    for(const auto& w: dict)
    {
        m_plugin->dict.insert(w);
        dico.push_back(QString::fromStdString(w));
    }
    KConfigGroup config(KSharedConfig::openConfig(),QString::fromStdString(m_plugin->defaults.config));
    config.writeEntry("on",m_plugin->on);
    config.writeEntry("auto_on",m_plugin->onAuto);
    config.writeEntry("server",m_plugin->serv);
    config.writeEntry("marxon",m_plugin->marxon);
    config.writeEntry("auto_marxon",m_plugin->marxonAuto);
    config.writeEntry("low_color",m_plugin->lowColor);
    config.writeEntry("mid_color",m_plugin->midColor);
    config.writeEntry("high_color",m_plugin->highColor);
    config.writeEntry("higher_color",m_plugin->higherColor);
    config.writeEntry("misc_color",m_plugin->miscColor);
    config.writeEntry("throttle",(int)m_plugin->throttle.count());
    config.writeEntry("max_suggestions",(int)m_plugin->sugMax);
    config.writeEntry("max_links",(int)m_plugin->linkMax);
    config.writeEntry("net_timeout",(int)m_plugin->netTimeout);
    config.writeEntry("dictionary",dico);
    emit m_plugin->signalParamsChange();
}

void KLanguageToolConfigWidget::reset(void)
{
    turnOn->setCheckState((m_plugin->on)?((m_plugin->onAuto)?(Qt::PartiallyChecked):(Qt::Checked)):(Qt::Unchecked));
    marxOn->setCheckState((m_plugin->marxon)?((m_plugin->marxonAuto)?(Qt::PartiallyChecked):(Qt::Checked)):(Qt::Unchecked));
    server->setText(m_plugin->serv);
    low->setCurrentColor(m_plugin->lowColor);
    mid->setCurrentColor(m_plugin->midColor);
    high->setCurrentColor(m_plugin->highColor);
    higher->setCurrentColor(m_plugin->higherColor);
    misc->setCurrentColor(m_plugin->miscColor);
    throttle->setValue(m_plugin->throttle.count());
    sugMax->setValue(m_plugin->sugMax);
    linkMax->setValue(m_plugin->linkMax);
    netTO->setValue(m_plugin->netTimeout);
    dict.clear();
    dictList->clear();
    for(const auto& w: m_plugin->dict)
    {
        dict.push_back(w);
    }
    std::stable_sort(dict.begin(),dict.end());
    for(const auto& w: dict)
    {
        dictList->addItem(QString::fromStdString(w));
    }
    slotUpdateLangs();
}

void KLanguageToolConfigWidget::defaults(void)
{
    turnOn->setCheckState((m_plugin->defaults.on)?((m_plugin->defaults.onAuto)?(Qt::PartiallyChecked):(Qt::Checked)):(Qt::Unchecked));
    marxOn->setCheckState((m_plugin->defaults.marxon)?((m_plugin->defaults.marxonAuto)?(Qt::PartiallyChecked):(Qt::Checked)):(Qt::Unchecked));
    server->setText(QString::fromStdString(m_plugin->defaults.server));
    std::string clang = KConfig().locale().toStdString();
    for(size_t i = 0; i < clang.size(); ++i)
    {
        if(clang[i] == '_')
        {
            clang[i] = '-';
        }
    }
    if(m_plugin->invertLangs.find(clang) == m_plugin->invertLangs.end())
    {
        clang = std::string(clang,0,clang.find('-'));
        if(m_plugin->invertLangs.find(clang) == m_plugin->invertLangs.end())
        {
            clang = m_plugin->invertLangs.begin()->first;
        }
    }
    langSelect->setCurrentText(QString::fromStdString(m_plugin->invertLangs[clang]));
    nativeLangSelect->setCurrentText(QString::fromStdString(m_plugin->invertLangs[clang]));
    dict.clear();
    dictList->clear();
    QPalette pal = lowb->palette();
    pal.setColor(QPalette::Button,m_plugin->defaults.lowColor);
    lowb->setAutoFillBackground(true);
    lowb->setPalette(pal);
    lowb->update();
    low ->setCurrentColor(m_plugin->defaults.lowColor);
    pal.setColor(QPalette::Button,m_plugin->defaults.midColor);
    midb->setPalette(pal);
    midb->update();
    mid->setCurrentColor(m_plugin->defaults.midColor);
    pal.setColor(QPalette::Button,m_plugin->defaults.highColor);
    highb->setPalette(pal);
    highb->update();
    high->setCurrentColor(m_plugin->defaults.highColor);
    pal.setColor(QPalette::Button,m_plugin->defaults.higherColor);
    higherb->setPalette(pal);
    higherb->update();
    higher->setCurrentColor(m_plugin->defaults.higherColor);
    pal.setColor(QPalette::Button,m_plugin->defaults.miscColor);
    miscb->setPalette(pal);
    miscb->update();
    misc->setCurrentColor(m_plugin->defaults.miscColor);
    throttle->setValue(m_plugin->defaults.throttle);
    sugMax->setValue(m_plugin->defaults.sugMax);
    linkMax->setValue(m_plugin->defaults.linkMax);
    netTO->setValue(m_plugin->defaults.netTimeout);
    emit changed();
}

void KLanguageToolConfigWidget::slotUpdateLangs(void)
{
    std::vector<std::string> sortedLangs;
    for(const auto& l: m_plugin->supportedLangs)
    {
        sortedLangs.push_back(l.first);
    }
    std::stable_sort(sortedLangs.begin(),sortedLangs.end());
    langSelect->clear();
    nativeLangSelect->clear();
    for(const auto& l: sortedLangs)
    {
        langSelect->addItem(QString::fromStdString(l));
        nativeLangSelect->addItem(QString::fromStdString(l));
    }
    langSelect->setCurrentText(QString::fromStdString(m_plugin->invertLangs[m_plugin->useLang.toStdString()]));
    nativeLangSelect->setCurrentText(QString::fromStdString(m_plugin->invertLangs[m_plugin->useNativeLang.toStdString()]));
}

void KLanguageToolConfigWidget::slotAddDict(void)
{
    QString s = dictList->currentText();
    if(s.size() > 0)
    {
        bool nfound = true;
        for(const auto& w: dict)
        {
            if(w == s.toStdString())
            {
                nfound = false;
                break;
            }
        }
        if(nfound)
        {
            dict.push_back(s.toStdString());
            std::stable_sort(dict.begin(),dict.end());
            dictList->clear();
            for(const auto& w: dict)
            {
                dictList->addItem(QString::fromStdString(w));
            }
            emit changed();
        }
    }
}

void KLanguageToolConfigWidget::slotRemDict(void)
{
    std::vector<std::string>::iterator it;
    for(it = dict.begin(); it < dict.end(); ++it)
    {
        if((*it) == dictList->currentText().toStdString())
        {
            break;
        }
    }
    dict.erase(it);
    dictList->removeItem(dictList->currentIndex());
    emit changed();
}

void KLanguageToolConfigWidget::slotClearDict(void)
{
    dict.clear();
    dictList->clear();
    emit changed();
}

void KLanguageToolConfigWidget::slotColorDialog(char c)
{
    switch(c)
    {
        case 'l':
            low->open(this,nullptr);
            break;
        case 'm':
            mid->open(this,nullptr);
            break;
        case 'h':
            high->open(this,nullptr);
            break;
        case 'i':
            higher->open(this,nullptr);
            break;
        case 'o':
            misc->open(this,nullptr);
            break;
        default:
            break;
    }
}

void KLanguageToolConfigWidget::slotColorChanged(char c, const QColor& color)
{
    QPalette pal;
    switch(c)
    {
        case 'l':
            pal = lowb->palette();
            pal.setColor(QPalette::Button,color);
            lowb->setAutoFillBackground(true);
            lowb->setPalette(pal);
            lowb->update();
            break;
        case 'm':
            pal = midb->palette();
            pal.setColor(QPalette::Button,color);
            midb->setAutoFillBackground(true);
            midb->setPalette(pal);
            midb->update();
            break;
        case 'h':
            pal = highb->palette();
            pal.setColor(QPalette::Button,color);
            highb->setAutoFillBackground(true);
            highb->setPalette(pal);
            highb->update();
            break;
        case 'i':
            pal = higherb->palette();
            pal.setColor(QPalette::Button,color);
            higherb->setAutoFillBackground(true);
            higherb->setPalette(pal);
            higherb->update();
            break;
        case 'o':
            pal = miscb->palette();
            pal.setColor(QPalette::Button,color);
            miscb->setAutoFillBackground(true);
            miscb->setPalette(pal);
            miscb->update();
            break;
        default:
            break;
    }
    emit changed();
}
