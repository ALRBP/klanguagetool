#ifndef KLANGUAGETOOL_VIEW_H
#define KLANGUAGETOOL_VIEW_H
#include <chrono>
#include <list>
#include <vector>
#include <QtCore/QString>
#include <QtCore/QPair>
#include <QtCore/QSet>
#include <QtNetwork/QNetworkReply>
#include <KTextEditor/MovingRange>
#include <KActionMenu>
#include <kxmlguiclient.h>
#include "klanguagetool.h++"
#include "tools/ltjson.h++"


struct KateMatch
{
    KTextEditor::MovingRange* range;
    KTextEditor::MovingRange* repRange;
    LTErrorLvl type;
    QString message;
    QString shortm;
    QList<QPair<QString,QString>> replace;
    QList<QUrl> urls;
};

struct KateBlock
{
    KTextEditor::MovingRange* range;
    std::list<size_t> matches;
    QString text;
};


class KLanguageToolView: public QObject, public KXMLGUIClient
{
Q_OBJECT

public:
    explicit KLanguageToolView(KLanguageTool* plugin, KTextEditor::MainWindow* mw);

    ~KLanguageToolView(void);

public slots:
    void slotDocChanged(void);
    
    void slotNeedCheck(void);
    
    void slotFullCheck(void);
    
    void slotRefreshCheck(void);
    
    void slotCheckReplied(QNetworkReply* reply, const std::vector<KTextEditor::MovingCursor*>& conv, const KateBlock& b1);

    void slotContextMenuAboutToShow(void);

    void updateLangs(void);

    void slotAddIgnore(const std::string& word);
    
    void slotRemIgnore(const std::string& word);

    void slotClearIngore(void);

    void slotUpdateToolMenu(void);

    void slotModeChange(QAction* l);

    void slotMarxonChange(QAction* l);

    void slotLangChange(QAction* l);

    void slotNativeLangChange(QAction* l);

private:
    KTextEditor::MainWindow* m_mainWindow;
    
    KLanguageTool* m_plugin;
    
    KActionMenu* m_popup;
    
    KActionMenu* m_tools;
    
    QActionGroup* modeSel;
    
    QActionGroup* marxonSel;
    
    QAction* servSync;
    
    QActionGroup* langSel;
    
    QActionGroup* nativeSel;
    
    std::chrono::steady_clock::time_point lastCheckTime;
    
    bool waitingToCheck;
    
    bool checkCanceled;
    
    bool fullCheck;
    
    unsigned waitingForReplies;
    
    std::list<KateBlock> blocks;
    
    std::list<KateBlock> tmpBlocks;
    
    std::vector<KateMatch> matches;
    
    std::vector<KateMatch> tmpMatches;
    
    std::unordered_set<std::string> ignore;
    
    QSet<QMetaObject::Connection*> docCon;
};
#endif
