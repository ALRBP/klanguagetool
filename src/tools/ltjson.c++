#include "ltjson.h++"


void from_json(const nlohmann::json& j, LTMatch& m)
{
    j.at("offset").get_to(m.offset);
    j.at("length").get_to(m.len);
    if(j.contains("message"))
    {
        j.at("message").get_to(m.message);
    }
    else
    {
        m.message = "";
    }
    if(j.contains("shortMessage"))
    {
        j.at("shortMessage").get_to(m.shortm);
    }
    else
    {
        m.shortm = "";
    }
    if(m.message.size() == 0)
    {
        if(m.shortm.size() > 0)
        {
            m.message = m.shortm;
        }
    }
    if(m.shortm.size() == 0)
    {
        if(m.message.size() > 0)
        {
            m.shortm = m.message;
        }
    }
    m.replace = std::list<std::pair<std::string,std::string>>();
    if(j.contains("replacements"))
    {
        for(const auto& jt: j["replacements"])
        {
            std::pair<std::string,std::string> r;
            jt.at("value").get_to(r.first);
            if(jt.contains("shortMessage"))
            {
                jt.at("shortMessage").get_to(r.second);
            }
            else
            {
                r.second = "";
            }
            m.replace.push_back(r);
        }
    }
    m.urls = std::list<std::string>();
    if(j["rule"].contains("urls"))
    {
        for(const auto& jt: j["rule"]["urls"])
        {
            std::string u;
            jt.at("value").get_to(u);
            m.urls.push_back(u);
        }
    }
    std::string cat;
    j["rule"].at("issueType").get_to(cat);//Source: https://www.w3.org/International/multilingualweb/lt/drafts/its20/its20.html#lqissue-typevalues TODO suggest +cultural
    if(cat == "terminology")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "mistranslation")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "omission")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "untranslated")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "addition")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "duplication")
    {
        m.type = LTErrorLvl::Mid;
    }
    else if(cat == "inconsistency")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "grammar")
    {
        m.type = LTErrorLvl::Mid;
    }
    else if(cat == "legal")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "register")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "locale-specific-content")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "locale-violation")
    {
        m.type = LTErrorLvl::High;
    }
    else if(cat == "style")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "characters")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "misspelling")
    {
        m.type = LTErrorLvl::Low;
    }
    else if(cat == "typographical")
    {
        m.type = LTErrorLvl::Mid;
    }
    else if(cat == "formatting")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "inconsistent-entities")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "numbers")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "markup")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "pattern-problem")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "whitespace")
    {
        m.type = LTErrorLvl::Mid;
    }
    else if(cat == "internationalization")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "length")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "non-conformance")
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "cultural")//Out of present standard - Describes terms "culturally (or politically) inappropriate" for given locale
    {
        m.type = LTErrorLvl::Higher;
    }
    else if(cat == "uncategorized")
    {
        m.type = LTErrorLvl::Misc;
    }
    else if(cat == "other")
    {
        m.type = LTErrorLvl::Misc;
    }
    else
    {
        m.type = LTErrorLvl::Misc;
    }
}
