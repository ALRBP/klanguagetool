#ifndef KLANGUAGETOOL_JSON_H
#define KLANGUAGETOOL_JSON_H
#include <string>
#include <utility>
#include <list>
#include <nlohmann/json.hpp>


enum class LTErrorLvl{Low,Mid,High,Higher,Misc};

struct LTMatch
{
    size_t offset;
    size_t len;
    LTErrorLvl type;
    std::string message;
    std::string shortm;
    std::list<std::pair<std::string,std::string>> replace;
    std::list<std::string> urls;
};


void from_json(const nlohmann::json& j, LTMatch& m);
#endif
