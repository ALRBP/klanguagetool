#ifndef KLANGUAGETOOL_PARAMS_H
#define KLANGUAGETOOL_PARAMS_H
#include <string>
#include <utility>
#include <list>
#include <unordered_map>
#include <QtGui/QColor>
#include <QtGui/QIcon>
#include <yaml-cpp/yaml.h>
#include <toml++/toml.hpp>


struct KLTDefaults
{
    std::string name;
    
    QIcon icon;
    
    std::string config;
    
    std::string id;
    
    std::string marxonFolder;

    bool on;
    
    bool onAuto;
    
    bool marxon;
    
    bool marxonAuto;
    
    bool onDefault;
    
    bool marxonDefault;
    
    std::string server;
    
    QColor lowColor;
    
    QColor midColor;
    
    QColor highColor;
    
    QColor higherColor;
    
    QColor miscColor;
    
    unsigned throttle;
    
    unsigned sugMax;
    
    unsigned linkMax;
    
    unsigned netTimeout;
    
    std::string checkPath;
    
    std::string langsPath;
    
    std::string noLang;
    
    bool invert;

    std::list<std::pair<std::string,std::string>> delimiters;
    
    std::string settingsName;
    
    unsigned maxInt;
};

struct KLTFormat
{
    bool on;
    
    bool marxon;
    
    std::string marxonName;
    
    bool invert;
    
    bool defDelim;

    std::list<std::pair<std::string,std::string>> delimiters;
};

namespace YAML
{
template<>
struct convert<KLTFormat>
{
    static Node encode(const KLTFormat& f)
    {
        Node node;
        if(f.on)
        {
            node["enable"] = Null;
        }
        if(f.marxon)
        {
            node["marxon"] = f.marxonName;
        }
        if(f.invert)
        {
            node["invert"] = Null;
        }
        if(!(f.defDelim))
        {
            node["invert"] = f.delimiters;
        }
        return node;
    }

    static bool decode(const Node& node, KLTFormat& f)
    {
        f.on = (node["enable"] && !(node["enable"].IsScalar() && !(node["enable"].as<bool>())));
        if(node["marxon"])
        {
            try
            {
                if(node["marxon"].IsScalar() && !(node["marxon"].as<bool>()))
                {
                    f.marxon = false;
                    f.marxonName = "";
                    
                }
                else
                {
                    f.marxon = true;
                    f.marxonName = node["marxon"].as<std::string>();
                }
            }
            catch(...)
            {
                f.marxon = true;
                f.marxonName = node["marxon"].as<std::string>();
            }
        }
        else
        {
            f.marxon = false;
            f.marxonName = "";
        }
        f.invert = (node["invert"] && !(node["invert"].IsScalar() && !(node["invert"].as<bool>())));
        if(node["delimiters"])
        {
            f.defDelim = false;
            f.delimiters = node["delimiters"].as<std::list<std::pair<std::string,std::string>>>();
        }
        else
        {
            f.defDelim = true;
        }
        return true;
    }
};
}

KLTDefaults getDefaults(const std::string& str);

std::unordered_map<std::string,KLTFormat> getFormats(const std::string& str, const std::list<std::pair<std::string,std::string>>& defaultDelimiters);

void getFormats(std::unordered_map<std::string,KLTFormat>& ans, const std::string& str, const std::list<std::pair<std::string,std::string>>& defaultDelimiters);
#endif
