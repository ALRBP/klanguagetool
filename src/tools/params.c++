#include <QtCore/QString>
#include "../utils/utils.h++"
#include "params.h++"

KLTDefaults getDefaults(const std::string& str)
{
    KLTDefaults ans;
    toml::table tbl = toml::parse(str);
    ans.name = tbl["plugin"]["name"].value<std::string>().value();
    ans.icon = QIcon::fromTheme(QString::fromStdString(tbl["plugin"]["icon"].value<std::string>().value()));
    ans.config = tbl["plugin"]["conf"].value<std::string>().value();
    ans.id = tbl["plugin"]["id"].value<std::string>().value();
    ans.marxonFolder = tbl["plugin"]["marxon_folder"].value<std::string>().value();
    ans.on = tbl["format_defaults"]["on"].value<bool>().value();
    ans.onAuto = tbl["format_defaults"]["auto_on"].value<bool>().value();
    ans.marxon = tbl["format_defaults"]["marxon"].value<bool>().value();
    ans.marxonAuto = tbl["format_defaults"]["auto_marxon"].value<bool>().value();
    ans.onDefault = tbl["format_defaults"]["default_on"].value<bool>().value();
    ans.marxonDefault = tbl["format_defaults"]["default_marxon"].value<bool>().value();
    ans.server = tbl["network"]["server"].value<std::string>().value();
    ans.lowColor = QColor(QString::fromStdString(tbl["color"]["low"].value<std::string>().value()));
    ans.midColor = QColor(QString::fromStdString(tbl["color"]["mid"].value<std::string>().value()));
    ans.highColor = QColor(QString::fromStdString(tbl["color"]["high"].value<std::string>().value()));
    ans.higherColor = QColor(QString::fromStdString(tbl["color"]["higher"].value<std::string>().value()));
    ans.miscColor = QColor(QString::fromStdString(tbl["color"]["misc"].value<std::string>().value()));
    ans.throttle = tbl["network"]["throttle"].value<unsigned>().value();
    ans.sugMax = tbl["interface"]["max_suggestions"].value<unsigned>().value();
    ans.linkMax = tbl["interface"]["max_links"].value<unsigned>().value();
    ans.netTimeout = tbl["network"]["timeout"].value<unsigned>().value();
    ans.checkPath = tbl["network"]["check"].value<std::string>().value();
    ans.langsPath = tbl["network"]["langs"].value<std::string>().value();
    ans.noLang = tbl["misc"]["no_lang"].value<std::string>().value();
    ans.invert = tbl["format_defaults"]["invert"].value<bool>().value();
    ans.settingsName = tbl["plugin"]["settings"].value<std::string>().value();
    ans.maxInt = tbl["misc"]["max_int"].value<unsigned>().value();
    toml::array a = *(tbl["format_defaults"]["delimiters"].as_array());
    for(const auto& d: a)
    {
        toml::array p = *(d.as_array());
        ans.delimiters.push_back(std::pair<std::string,std::string>(p[0].value<std::string>().value(),p[1].value<std::string>().value()));
    }
    return ans;
}

std::unordered_map<std::string,KLTFormat> getFormats(const std::string& str, const std::list<std::pair<std::string,std::string>>& defaultDelimiters)
{
    YAML::Node def = YAML::Load(str);
    std::unordered_map<std::string,KLTFormat> ans = def.as<std::unordered_map<std::string,KLTFormat>>();
    for(auto& f: ans)
    {
        if(f.second.defDelim)
        {
            f.second.delimiters = defaultDelimiters;
        }
    }
    return ans;
}

void getFormats(std::unordered_map<std::string,KLTFormat>& ans, const std::string& str, const std::list<std::pair<std::string,std::string>>& defaultDelimiters)
{
    YAML::Node def = YAML::Load(str);
    for(const auto& d: def)
    {
        KLTFormat f = d.second.as<KLTFormat>();
        if(f.defDelim)
        {
            f.delimiters = defaultDelimiters;
        }
        ans[d.first.as<std::string>()] = f;
    }
}
